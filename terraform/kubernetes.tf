terraform {
    required_version = ">= 0.9.3"
    backend "s3" {
        bucket = "nickd-terraform-state",
    	key = "kubernetes.tfstate",
        region = "us-east-1",
        dynamodb_table = "terraform_locktable"
    }
}

output "cluster_name" {
  value = "k8s.graphaspect.com"
}

output "master_security_group_ids" {
  value = ["${aws_security_group.masters-k8s-graphaspect-com.id}"]
}

output "masters_role_arn" {
  value = "${aws_iam_role.masters-k8s-graphaspect-com.arn}"
}

output "masters_role_name" {
  value = "${aws_iam_role.masters-k8s-graphaspect-com.name}"
}

output "node_security_group_ids" {
  value = ["${aws_security_group.nodes-k8s-graphaspect-com.id}"]
}

output "node_subnet_ids" {
  value = ["${aws_subnet.us-east-1a-k8s-graphaspect-com.id}"]
}

output "nodes_role_arn" {
  value = "${aws_iam_role.nodes-k8s-graphaspect-com.arn}"
}

output "nodes_role_name" {
  value = "${aws_iam_role.nodes-k8s-graphaspect-com.name}"
}

output "region" {
  value = "us-east-1"
}

output "vpc_id" {
  value = "${aws_vpc.k8s-graphaspect-com.id}"
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_autoscaling_group" "master-us-east-1a-masters-k8s-graphaspect-com" {
  name                 = "master-us-east-1a.masters.k8s.graphaspect.com"
  launch_configuration = "${aws_launch_configuration.master-us-east-1a-masters-k8s-graphaspect-com.id}"
  max_size             = 1
  min_size             = 1
  vpc_zone_identifier  = ["${aws_subnet.us-east-1a-k8s-graphaspect-com.id}"]

  tag = {
    key                 = "KubernetesCluster"
    value               = "k8s.graphaspect.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "Name"
    value               = "master-us-east-1a.masters.k8s.graphaspect.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/instancegroup"
    value               = "master-us-east-1a"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/role/master"
    value               = "1"
    propagate_at_launch = true
  }
}

resource "aws_autoscaling_group" "nodes-k8s-graphaspect-com" {
  name                 = "nodes.k8s.graphaspect.com"
  launch_configuration = "${aws_launch_configuration.nodes-k8s-graphaspect-com.id}"
  max_size             = 1
  min_size             = 1
  vpc_zone_identifier  = ["${aws_subnet.us-east-1a-k8s-graphaspect-com.id}"]

  tag = {
    key                 = "KubernetesCluster"
    value               = "k8s.graphaspect.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "Name"
    value               = "nodes.k8s.graphaspect.com"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/cluster-autoscaler/node-template/label/kops.k8s.io/instancegroup"
    value               = "nodes"
    propagate_at_launch = true
  }

  tag = {
    key                 = "k8s.io/role/node"
    value               = "1"
    propagate_at_launch = true
  }
}

resource "aws_ebs_volume" "a-etcd-events-k8s-graphaspect-com" {
  availability_zone = "us-east-1a"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster    = "k8s.graphaspect.com"
    Name                 = "a.etcd-events.k8s.graphaspect.com"
    "k8s.io/etcd/events" = "a/a"
    "k8s.io/role/master" = "1"
  }
}

resource "aws_ebs_volume" "a-etcd-main-k8s-graphaspect-com" {
  availability_zone = "us-east-1a"
  size              = 20
  type              = "gp2"
  encrypted         = false

  tags = {
    KubernetesCluster    = "k8s.graphaspect.com"
    Name                 = "a.etcd-main.k8s.graphaspect.com"
    "k8s.io/etcd/main"   = "a/a"
    "k8s.io/role/master" = "1"
  }
}

resource "aws_iam_instance_profile" "masters-k8s-graphaspect-com" {
  name = "masters.k8s.graphaspect.com"
  role = "${aws_iam_role.masters-k8s-graphaspect-com.name}"
}

resource "aws_iam_instance_profile" "nodes-k8s-graphaspect-com" {
  name = "nodes.k8s.graphaspect.com"
  role = "${aws_iam_role.nodes-k8s-graphaspect-com.name}"
}

resource "aws_iam_role" "masters-k8s-graphaspect-com" {
  name               = "masters.k8s.graphaspect.com"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_masters.k8s.graphaspect.com_policy")}"
}

resource "aws_iam_role" "nodes-k8s-graphaspect-com" {
  name               = "nodes.k8s.graphaspect.com"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role_nodes.k8s.graphaspect.com_policy")}"
}

resource "aws_iam_role_policy" "masters-k8s-graphaspect-com" {
  name   = "masters.k8s.graphaspect.com"
  role   = "${aws_iam_role.masters-k8s-graphaspect-com.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_masters.k8s.graphaspect.com_policy")}"
}

resource "aws_iam_role_policy" "nodes-k8s-graphaspect-com" {
  name   = "nodes.k8s.graphaspect.com"
  role   = "${aws_iam_role.nodes-k8s-graphaspect-com.name}"
  policy = "${file("${path.module}/data/aws_iam_role_policy_nodes.k8s.graphaspect.com_policy")}"
}

resource "aws_internet_gateway" "k8s-graphaspect-com" {
  vpc_id = "${aws_vpc.k8s-graphaspect-com.id}"

  tags = {
    KubernetesCluster = "k8s.graphaspect.com"
    Name              = "k8s.graphaspect.com"
  }
}

resource "aws_key_pair" "kubernetes-k8s-graphaspect-com-640e936ed939a514d40f3c213009fcc9" {
  key_name   = "kubernetes.k8s.graphaspect.com-64:0e:93:6e:d9:39:a5:14:d4:0f:3c:21:30:09:fc:c9"
  public_key = "${file("${path.module}/data/aws_key_pair_kubernetes.k8s.graphaspect.com-640e936ed939a514d40f3c213009fcc9_public_key")}"
}

resource "aws_launch_configuration" "master-us-east-1a-masters-k8s-graphaspect-com" {
  name_prefix                 = "master-us-east-1a.masters.k8s.graphaspect.com-"
  image_id                    = "ami-8ec0e1f4"
  instance_type               = "t2.micro"
  key_name                    = "${aws_key_pair.kubernetes-k8s-graphaspect-com-640e936ed939a514d40f3c213009fcc9.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.masters-k8s-graphaspect-com.id}"
  security_groups             = ["${aws_security_group.masters-k8s-graphaspect-com.id}"]
  associate_public_ip_address = true
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_master-us-east-1a.masters.k8s.graphaspect.com_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 64
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_launch_configuration" "nodes-k8s-graphaspect-com" {
  name_prefix                 = "nodes.k8s.graphaspect.com-"
  image_id                    = "ami-8ec0e1f4"
  instance_type               = "t2.micro"
  key_name                    = "${aws_key_pair.kubernetes-k8s-graphaspect-com-640e936ed939a514d40f3c213009fcc9.id}"
  iam_instance_profile        = "${aws_iam_instance_profile.nodes-k8s-graphaspect-com.id}"
  security_groups             = ["${aws_security_group.nodes-k8s-graphaspect-com.id}"]
  associate_public_ip_address = true
  user_data                   = "${file("${path.module}/data/aws_launch_configuration_nodes.k8s.graphaspect.com_user_data")}"

  root_block_device = {
    volume_type           = "gp2"
    volume_size           = 128
    delete_on_termination = true
  }

  lifecycle = {
    create_before_destroy = true
  }
}

resource "aws_route" "0-0-0-0--0" {
  route_table_id         = "${aws_route_table.k8s-graphaspect-com.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.k8s-graphaspect-com.id}"
}

resource "aws_route_table" "k8s-graphaspect-com" {
  vpc_id = "${aws_vpc.k8s-graphaspect-com.id}"

  tags = {
    KubernetesCluster = "k8s.graphaspect.com"
    Name              = "k8s.graphaspect.com"
  }
}

resource "aws_route_table_association" "us-east-1a-k8s-graphaspect-com" {
  subnet_id      = "${aws_subnet.us-east-1a-k8s-graphaspect-com.id}"
  route_table_id = "${aws_route_table.k8s-graphaspect-com.id}"
}

resource "aws_security_group" "masters-k8s-graphaspect-com" {
  name        = "masters.k8s.graphaspect.com"
  vpc_id      = "${aws_vpc.k8s-graphaspect-com.id}"
  description = "Security group for masters"

  tags = {
    KubernetesCluster = "k8s.graphaspect.com"
    Name              = "masters.k8s.graphaspect.com"
  }
}

resource "aws_security_group" "nodes-k8s-graphaspect-com" {
  name        = "nodes.k8s.graphaspect.com"
  vpc_id      = "${aws_vpc.k8s-graphaspect-com.id}"
  description = "Security group for nodes"

  tags = {
    KubernetesCluster = "k8s.graphaspect.com"
    Name              = "nodes.k8s.graphaspect.com"
  }
}

resource "aws_security_group_rule" "all-master-to-master" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-k8s-graphaspect-com.id}"
  source_security_group_id = "${aws_security_group.masters-k8s-graphaspect-com.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "all-master-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes-k8s-graphaspect-com.id}"
  source_security_group_id = "${aws_security_group.masters-k8s-graphaspect-com.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "all-node-to-node" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.nodes-k8s-graphaspect-com.id}"
  source_security_group_id = "${aws_security_group.nodes-k8s-graphaspect-com.id}"
  from_port                = 0
  to_port                  = 0
  protocol                 = "-1"
}

resource "aws_security_group_rule" "https-external-to-master-0-0-0-0--0" {
  type              = "ingress"
  security_group_id = "${aws_security_group.masters-k8s-graphaspect-com.id}"
  from_port         = 443
  to_port           = 443
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "master-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.masters-k8s-graphaspect-com.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-egress" {
  type              = "egress"
  security_group_id = "${aws_security_group.nodes-k8s-graphaspect-com.id}"
  from_port         = 0
  to_port           = 0
  protocol          = "-1"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "node-to-master-tcp-1-2379" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-k8s-graphaspect-com.id}"
  source_security_group_id = "${aws_security_group.nodes-k8s-graphaspect-com.id}"
  from_port                = 1
  to_port                  = 2379
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-tcp-2382-4000" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-k8s-graphaspect-com.id}"
  source_security_group_id = "${aws_security_group.nodes-k8s-graphaspect-com.id}"
  from_port                = 2382
  to_port                  = 4000
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-tcp-4003-65535" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-k8s-graphaspect-com.id}"
  source_security_group_id = "${aws_security_group.nodes-k8s-graphaspect-com.id}"
  from_port                = 4003
  to_port                  = 65535
  protocol                 = "tcp"
}

resource "aws_security_group_rule" "node-to-master-udp-1-65535" {
  type                     = "ingress"
  security_group_id        = "${aws_security_group.masters-k8s-graphaspect-com.id}"
  source_security_group_id = "${aws_security_group.nodes-k8s-graphaspect-com.id}"
  from_port                = 1
  to_port                  = 65535
  protocol                 = "udp"
}

resource "aws_security_group_rule" "ssh-external-to-master-0-0-0-0--0" {
  type              = "ingress"
  security_group_id = "${aws_security_group.masters-k8s-graphaspect-com.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_security_group_rule" "ssh-external-to-node-0-0-0-0--0" {
  type              = "ingress"
  security_group_id = "${aws_security_group.nodes-k8s-graphaspect-com.id}"
  from_port         = 22
  to_port           = 22
  protocol          = "tcp"
  cidr_blocks       = ["0.0.0.0/0"]
}

resource "aws_subnet" "us-east-1a-k8s-graphaspect-com" {
  vpc_id            = "${aws_vpc.k8s-graphaspect-com.id}"
  cidr_block        = "172.20.32.0/19"
  availability_zone = "us-east-1a"

  tags = {
    KubernetesCluster                           = "k8s.graphaspect.com"
    Name                                        = "us-east-1a.k8s.graphaspect.com"
    "kubernetes.io/cluster/k8s.graphaspect.com" = "owned"
    "kubernetes.io/role/elb"                    = "1"
  }
}

resource "aws_vpc" "k8s-graphaspect-com" {
  cidr_block           = "172.20.0.0/16"
  enable_dns_hostnames = true
  enable_dns_support   = true

  tags = {
    KubernetesCluster                           = "k8s.graphaspect.com"
    Name                                        = "k8s.graphaspect.com"
    "kubernetes.io/cluster/k8s.graphaspect.com" = "owned"
  }
}

resource "aws_vpc_dhcp_options" "k8s-graphaspect-com" {
  domain_name         = "ec2.internal"
  domain_name_servers = ["AmazonProvidedDNS"]

  tags = {
    KubernetesCluster = "k8s.graphaspect.com"
    Name              = "k8s.graphaspect.com"
  }
}

resource "aws_vpc_dhcp_options_association" "k8s-graphaspect-com" {
  vpc_id          = "${aws_vpc.k8s-graphaspect-com.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.k8s-graphaspect-com.id}"
}

